# Tryond Demo project - Transactions Manager
This is a demo project - part of the interview process
# Setup
  - Clone the project
  - Copy .env.example to .env
  - Set the database .env file
```env
DB_CONNECTION=mysql
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
  - Install composer packages
```sh
composer install
```
  - Run migrations 
```sh
php artisan migrate
```
  - Run database seeders 
```sh
php artisan db:seed