@extends('layout.app')
@section('content')
<div id="app" class="container">
    <h1>Transactions Manager</h1>
    <trns-new-transaction></trns-new-transaction>
    <trns-transactions-table></trns-transactions-table>
    <div>
        <b-button v-b-modal.new_transaction>New transaction</b-button>
    </div>
</div>
@endsection
