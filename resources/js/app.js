require('./bootstrap');

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



import { library } from '@fortawesome/fontawesome-free'

Vue.use(BootstrapVue)

Vue.component('trns-change-mode', require('./components/ChangeModeComponent.vue').default)
Vue.component('trns-new-transaction', require('./components/NewTransactionComponent.vue').default)
Vue.component('trns-transactions-table', require('./components/TransactionsTableComponent.vue').default)

new Vue({
    el: '#app',
    data: {}
})