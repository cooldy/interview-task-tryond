<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_info = [
            [
                'name' => 'Rick',
                'email' => 'rick@rm.tv',
            ],
            [
                'name' => 'Morty',
                'email' => 'morty@rm.tv',
            ],
            [
                'name' => 'Jerry',
                'email' => 'jerry@rm.tv',
            ],
            [
                'name' => 'Beth',
                'email' => 'beth@rm.tv',
            ],
            [
                'name' => 'Summer',
                'email' => 'summer@rm.tv',
            ],
            [
                'name' => 'Mr. Meeseeks',
                'email' => 'meeseeks@rm.tv',
            ],
            [
                'name' => 'Birdperson',
                'email' => 'birdperson@rm.tv',
            ],
            [
                'name' => 'Unity',
                'email' => 'unity@rm.tv',
            ],
            [
                'name' => 'Squanchy',
                'email' => 'squanchy@rm.tv',
            ],
            [
                'name' => 'Scary Terry',
                'email' => 'scaryterry@rm.tv',
            ],
        ];

        foreach ($user_info as $key => $user) {
            $user_id = DB::table('users')->insertGetId([
                'name' => $user['name'],
                'email' => $user['email'],
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()'),
            ]);
            DB::table('user_transaction_accounts')->insert([
                'user_id' => $user_id,
                'balance' => 0,
                'max_credit' => rand(0, 1000), //making things fun
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()'),
            ]);
        }
    }
}
