<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StarterController@index');
Route::get('/transactions', 'TransactionsController@index');
Route::post('/transactions', 'TransactionsController@store');
Route::delete('/transactions/{id}', 'TransactionsController@destroy');
Route::put('/users/{id}', 'UsersController@update');
Route::get('/users', 'UsersController@index');
