<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTransactionAccountModel extends Model
{
    protected $table = 'user_transaction_accounts';

    public function user()
    {
        return  $this->belongsTo(User::class, 'user_id', 'id');
    }
}
