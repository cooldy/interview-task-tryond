<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    const TYPE_DEBIT = 1;
    const TYPE_CREDIT = 2;
    const TYPE_ARR = [
        self::TYPE_CREDIT,
        self::TYPE_DEBIT
    ];
    protected $table = 'transactions';
    protected $fillable = [
        'account_id', 'amount', 'type'
    ];

    public function account()
    {
        return $this->belongsTo(UserTransactionAccountModel::class, 'account_id', 'id');
    }

    public static function isDebit($type)
    {
        return self::TYPE_DEBIT == $type;
    }

    public static function isCredit($type)
    {
        return self::TYPE_CREDIT == $type;
    }
}
