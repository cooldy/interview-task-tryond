<?php

namespace App\Finance;

use App\TransactionModel;
use App\User;
use App\UserTransactionAccountModel;
use Illuminate\Support\Facades\DB;

class UserFinanceManager
{
    // User $user
    private $user;
    // TransactionModel  $transaction
    private $transaction;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function deposit($amount)
    {
        DB::beginTransaction();
        try {
            $this->createTransaction($amount, TransactionModel::TYPE_CREDIT);
            $this->recalculateUserBalance();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function withdraw($amount)
    {
        if ($this->exceedsCreditLimit($this->user->account->balance, $amount)) {
            throw new \Exception('User exceeds credit limit!', 1);
        }

        DB::beginTransaction();
        try {
            $this->createTransaction($amount, TransactionModel::TYPE_DEBIT);
            $this->recalculateUserBalance();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Used to delete a transaction and rollback its impact
     *
     * This could be done in a lot of ways but we'll be simply delete the transaction and recalculate the balance of the account
     *
     * @param TransactionModel $transaction
     * @return void
     */
    public function rollback(TransactionModel $transaction)
    {
        if (!$this->isDeletable($transaction)) {
            throw new \Exception('Transaction not deletable because .. reasons ', 1);
        }

        DB::beginTransaction();
        try {
            $transaction->delete();
            $this->recalculateUserBalance();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Check if the user's limit is exceeded.
     * Not in the inital scope but used to show why the UserFinanceManager is structured like this
     *
     * @param float $balance
     * @param float $amount
     * @return bool
     */
    private function exceedsCreditLimit($balance, $amount)
    {
        $account = $this->user->account;
        if ($amount > $balance) {
            if (abs($balance - $amount) > $account->max_credit) {
                return true;
            }
        }
        return false;
    }

    private function createTransaction($amount, $type)
    {
        return TransactionModel::create([
            'account_id' => $this->user->account->id,
            'amount' => $amount,
            'type' => $type
        ]);
    }

    private function recalculateUserBalance()
    {
        $user_transactions = TransactionModel::selectRaw('sum(amount) as sum_amount, type')->where('account_id', $this->user->account->id)->groupBy('type')->get();
        $credit_obj = $user_transactions->where('type', TransactionModel::TYPE_CREDIT)->first();
        $debit_obj = $user_transactions->where('type', TransactionModel::TYPE_DEBIT)->first();
        $credit = $credit_obj ? $credit_obj->sum_amount : 0;
        $debit = $debit_obj ? $debit_obj->sum_amount : 0;
        $balance = $credit - $debit;
        UserTransactionAccountModel::whereId($this->user->account->id)->update([
            'balance' => $balance
        ]);
    }

    /**
     * Checks if its possible to delete the transacion
     *
     * @param TransactionModel $transaction
     * @return boolean
     */
    private function isDeletable(TransactionModel $transaction)
    {
        //we'd most probably have a few checks we must do before deleting the transaction
        //also things like what would happen if after deleting the transaction the user's credit exceeds its limits

        //but ... that's irrelevant cause everything is possible with CORRUPTION

        $corruption = true;
        return $corruption;
    }
}
