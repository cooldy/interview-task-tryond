<?php

namespace App\Http\Transformers;

class TransactionsTransformer implements TransformerInterface
{
    public function transformCollection($transactions)
    {
        return array_map([$this, 'transformItem'], iterator_to_array($transactions));
    }

    public function transformItem($transaction)
    {
        $account = $transaction->account;
        return [
            'id' => $transaction->id,
            'user_id' => $account->user->id,
            'username' => $account->user->name,
            'email' => $account->user->email,
            'balance' => $account->balance,
            'amount' => $transaction->amount,
            'type' => $transaction->type,
        ];
    }
}
