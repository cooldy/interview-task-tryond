<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function update($user_id, Request $request)
    {
        $this->validate($request, ['username' => 'required|min:4|max:40']);
        User::where('id', $user_id)->update(['name' => $request->get('username')]);
    }

    public function index()
    {
        $users = User::select('name', 'id')->get();
        return array_map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
            ];
        }, iterator_to_array($users));
    }
}
