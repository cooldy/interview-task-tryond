<?php

namespace App\Http\Controllers;

use App\Finance\UserFinanceManager;
use App\Http\Transformers\TransactionsTransformer;
use App\TransactionModel;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Request;

class TransactionsController extends Controller
{
    public function index(TransactionsTransformer $transformer)
    {
        $transactions = TransactionModel::with(['account' => function ($q) {
            return $q->with('user');
        }])->get();
        return $transformer->transformCollection($transactions);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric|min:0.001',
            'type' => ['required', Rule::in(TransactionModel::TYPE_ARR)],
            'user_id' => 'required|exists:users,id'
        ]);
        $user = User::whereId($request->get('user_id'))->with('account')->first();
        $manager = new UserFinanceManager($user);

        try {
            if (TransactionModel::isCredit($request->get('type'))) {
                $manager->deposit($request->get('amount'));
            } elseif (TransactionModel::isDebit($request->get('type'))) {
                $manager->withdraw($request->get('amount'));
            } else {
                // laravel validator didnt do shit so... panic
            }
        } catch (\Throwable $th) {
            throw ValidationException::withMessages([
                [$th->getMessage()],
            ]);
        }
        return [
            'success' => true
        ];
    }

    public function destroy($id)
    {
        $transaction = TransactionModel::with('account')->whereId($id)->first();
        if (!$transaction) {
            throw ValidationException::withMessages([
                ['Invalid transaction ID']
            ]);
        }
        $user = User::whereId($transaction->account->user_id)->with('account')->first();
        $manager = new UserFinanceManager($user);
        $manager->rollback($transaction);
        return [
            'success' => true
        ];
    }
}
